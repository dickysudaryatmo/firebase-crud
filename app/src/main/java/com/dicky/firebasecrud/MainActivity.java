package com.dicky.firebasecrud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    EditText edId, edName;
    Button btnInsert;
    Button btnDisplay;

    FirebaseDatabase database;
    DatabaseReference myRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("message");
//
//        myRef.setValue("Hello, World!");
        edId = findViewById(R.id.id);
        edName = findViewById(R.id.name);

        btnInsert = findViewById(R.id.btnInsert);
        btnDisplay = findViewById(R.id.btnDisplay);
        btnDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,DisplayActivity.class));
            }
        });

        insertData();
    }

    private void insertData(){
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Data");

    btnInsert.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String id = edId.getText().toString();
            String name = edName.getText().toString();
            DataItem dataItem = new DataItem(id, name);
            // Add Timestamp
            long mDateTime = 9999999999999L - System.currentTimeMillis();
            String mOrderTime =  String.valueOf(mDateTime);
            myRef.child(mOrderTime).setValue(dataItem).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(),"Success", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),"Fail", Toast.LENGTH_SHORT).show();
                }
            });
        }
    });
    }
}